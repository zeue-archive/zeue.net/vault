FROM node:10-alpine

COPY . ./vault.zeue.net

RUN cd vault.zeue.net && npm i

EXPOSE 80/tcp

CMD cd vault.zeue.net && node_modules/.bin/micro -l tcp://0.0.0.0:80 index.js

