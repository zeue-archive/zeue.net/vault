const handler = require('serve-handler');

module.exports = async (request, response) => {
  await handler(request, response, {
  	"public" : "_public",
  	"cleanUrls" : false,
  	"unlisted" : [ ".unlisted" ]
  });
};
